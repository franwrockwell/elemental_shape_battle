How to play:
This is a two player game.
Each player on their turn clicks their units, then moves them one space with WASD.
P1's units are squares.
P2's units are diamonds.
At the end of a player's turn, their units will deal damage to any adjacent enemy units.
This damage is determined by which has an element advantage over the other.
Grenade spawn, and can be picked up by units.
When a unit with a grenade is selected, they can throw it with Q.
Grenades fly towards the mouse cursor.
Any units caught in the blast will change their element to whatever element the grenade was.
A player wins when all enemy units are defeated.